﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Prezzybox.DeveloperTest.Data.Tests.Requests
{
    public class SingleUserRequestTests
    {
        [Fact]
        public void ShouldGetUser()
        {
            var request = new Data.Requests.SingleUserRequest(2);

            //Assert.Equal(2, response.Id);
            //Assert.Equal("janet.weaver@reqres.in", response.Email);
        }

        [Fact]
        public void ShouldGetNullWhenNotFound()
        {
            // when a user is not found, the response should be null
            throw new NotImplementedException();
        }

        [Fact]
        public void ShouldGetPageOfDataWithUserIdOn()
        {
            var response = Data.Requests.ListUsersRequest.GetPageWithUser(3, 12).Result;

            Assert.Contains(response.Data, (u) => u.Id == 12);
            Assert.Equal(3, response.Data.Count);

            response = Data.Requests.ListUsersRequest.GetPageWithUser(2, 6).Result;
            Assert.Contains(response.Data, (u) => u.Id == 6);
            Assert.Equal(2, response.Data.Count);
        }
    }
}
