﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Prezzybox.DeveloperTest.Data.Tests.Requests
{
    public class ListUsersRequestTests
    {
        [Fact]
        public void ShouldGetUsersOnGivenPage()
        {
            var request = new Data.Requests.ListUsersRequest(2, 10);
            //Assert.Equal(2, response.Page)
            //Assert.Equal(10, response.PerPage)
        }

        [Fact]
        public void ShouldGetPageOfDataWithUserIdOn()
        {
            var response = Data.Requests.ListUsersRequest.GetPageWithUser(3, 12);
            //Assert.Contains(response.Data, (u) => u.Id == 12);
            //Assert.Equal(3, response.Data.Count);

            response = Data.Requests.ListUsersRequest.GetPageWithUser(2, 6);
            //Assert.Contains(response.Data, (u) => u.Id == 6);
            //Assert.Equal(2, response.Data.Count);
        }

        [Fact]
        public void ShouldGetCSVStream()
        {
            var response = new Data.Models.PagedList<Models.User>()
            {
                Data = new List<Models.User>() {
                    new Models.User() { FirstName = "f1", LastName = "l1", Email = "f1@example.net", Id = 1 },
                    new Models.User() { FirstName = "f2", LastName = "l2", Email = "f2@example.net", Id = 2 }
               }
            };

            var bytes = response.ToCsv();

            // TODO: assert that the CSV parses as a CSV and contains the data we expect

        }

    }
}
