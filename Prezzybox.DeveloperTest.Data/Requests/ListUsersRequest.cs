﻿using Prezzybox.DeveloperTest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Prezzybox.DeveloperTest.Data.Requests
{
    public class ListUsersRequest : Shared.RequestBase<Models.PagedList<Models.User>>
    {
        public ListUsersRequest(int page, int perPage)
        { }

        public override Task<PagedList<User>> GetResponse()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a page of results which contains the user with the given id
        /// </summary>
        /// <param name="perPage">How many items per page</param>
        /// <param name="id">The id we are searching for</param>
        /// <returns>Page of results containing the users id</returns>
        public static Task<PagedList<User>> GetPageWithUser(int perPage, int id)
        {
            //NOTE: assume that the API will not return IDs in sequential order
            throw new NotImplementedException();
        }

        /// <summary>
        /// Takes the results and converts them to an in memory CSV
        /// </summary>
        /// <returns>CSV data</returns>
        public static Task<byte[]> ToCsv()
        {

        }
    }
}
