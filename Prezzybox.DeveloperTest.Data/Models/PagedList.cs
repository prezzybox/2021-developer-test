﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Prezzybox.DeveloperTest.Data.Models
{
    /// <summary>
    /// A page of listed data
    /// </summary>
    /// <typeparam name="T">Type of listed data being fetched</typeparam>
    public class PagedList<T>
    {
        /// <summary>
        /// Gets or sets current page
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Gets or sets how many items are on each page
        /// </summary>
        public int PerPage { get; set; }

        /// <summary>
        /// Gets or sets total number of items in entire list
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Gets or sets total number of pages available
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Gets or sets current page of data
        /// </summary>
        public List<T> Data { get; set; }

        /// <summary>
        /// Takes the results and converts them to an in memory CSV
        /// </summary>
        /// <returns>CSV data</returns>
        public byte[] ToCsv()
        {
            throw new NotImplementedException();
        }
    }
}
