# About The Project

This solution is a sample start for middleware which uses the https://reqres.in/ API. It has 2 projects:

## Prezzybox.DeveloperTest.Data

The middleware layer for interacting with the API

## Prezzybox.DeveloperTest.Data.Tests

An xunit based test project for unit testing the middleware

# What you need to do

Your task is to take this solution (fork it or clone into a new repo) and "finish" it for the following operations:

* Get List Users
* Get Single User
* Create User

Expectations are that you: 

* follow the rough design patterns as already started
* expand on and create unit tests for all operations
* all existing tests must be developed into a passing state
* create code that you would consider production ready

Restrictions:

* you may not use https://www.nuget.org/packages/RestApiEngine or similar
* you may use other nuget packages as you see fit

# Submission + Questions

Please email (or preferably slack connect) me at craig.lager@prezzybox.com. Do not feel shy about any questions you have, especially if you're not sure what we're expecting or if you're not sure if you can/can not do a certain thing.